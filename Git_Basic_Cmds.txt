Creating Local Repo :
$mkdir gitrepo
$cd gitrepo
$git init .
$cd .git
$ cat HEAD
ref: refs/heads/25042017
note: here 25042017 is a branch name.
$ cat refs/heads/25042017
c5cf314019eda60f5ed860796af84d76e93f66c0

Note: it is latest commit number.

to check, hit log command
$ git log
commit c5cf314019eda60f5ed860796af84d76e93f66c0
Author: satish <satishdevops@hotmail.com>
Date:   Mon Apr 24 18:33:45 2017 +0530

################### HOOKS  #########################
we keep automaged scripts in 
.git/hooks (GIT_DIR!)
$ pwd
/c/Users/satish/Google Drive/Education/DEVOPS/GIT/kb/.git/hooks

#####################################################

$ git status -s
?? Git_Basic_Cmds.txt
It will show changed files.

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git add Git_Basic_Cmds.txt

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git status
On branch 25042017
Your branch is up-to-date with 'origin/25042017'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   Git_Basic_Cmds.txt


satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git commit -m "initial commit" Git_Basic_Cmds.txt
[25042017 6ef9362] initial commit
 1 file changed, 33 insertions(+)
 create mode 100644 Git_Basic_Cmds.txt

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git status
On branch 25042017
Your branch is ahead of 'origin/25042017' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean

$ git push
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 684 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote:
remote: Create pull request for 25042017:
remote:   https://bitbucket.org/satishdevops/kb/pull-requests/new?source=25042017&t=1
remote:
To https://bitbucket.org/satishdevops/kb.git
   c5cf314..6ef9362  25042017 -> 25042017

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git status -s


Again Modify File

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git status -s
 M Git_Basic_Cmds.txt


  
$ git show 6ef9362b74e15bc74f066cd1b66fd6691a2b8537
		commit 6ef9362b74e15bc74f066cd1b66fd6691a2b8537
		Author: satish <satishdevops@hotmail.com>
		Date:   Tue Apr 25 08:27:45 2017 +0530

			initial commit

		diff --git a/Git_Basic_Cmds.txt b/Git_Basic_Cmds.txt
		new file mode 100644
		index 0000000..aa92d1a
		--- /dev/null
		+++ b/Git_Basic_Cmds.txt
		@@ -0,0 +1,33 @@
		+Creating Local Repo :
		+$mkdir gitrepo
		+$cd gitrepo
		+$git init .
		+$cd .git
		+$ cat HEAD
		+ref: refs/heads/25042017
		+note: here 25042017 is a branch name.
		+$ cat refs/heads/25042017
		+c5cf314019eda60f5ed860796af84d76e93f66c0
		+
		+Note: it is latest commit number.
		+
		+to check, hit log command
		+$ git log
		+commit c5cf314019eda60f5ed860796af84d76e93f66c0
		+Author: satish <satishdevops@hotmail.com>
		+Date:   Mon Apr 24 18:33:45 2017 +0530
		+
		+################### HOOKS  #########################
		+we keep automaged scripts in
		+.git/hooks (GIT_DIR!)
		+$ pwd
		+/c/Users/satish/Google Drive/Education/DEVOPS/GIT/kb/.git/hooks
		+
		+#####################################################
		+
		+$ git status -s
		+?? Git_Basic_Cmds.txt
		+It will show changed files.
		+

$ git diff
		diff --git a/Git_Basic_Cmds.txt b/Git_Basic_Cmds.txt
		index aa92d1a..af8d925 100644
		--- a/Git_Basic_Cmds.txt
		+++ b/Git_Basic_Cmds.txt
		@@ -29,5 +29,43 @@ $ git status -s
		 ?? Git_Basic_Cmds.txt
		 It will show changed files.

		+satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
		+$ git add Git_Basic_Cmds.txt
		+
		+satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
		+$ git status
		+On branch 25042017
		+Your branch is up-to-date with 'origin/25042017'.
		+Changes to be committed:
		+  (use "git reset HEAD <file>..." to unstage)
		+
		+        new file:   Git_Basic_Cmds.txt
		+
		+
		+satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
		+$ git commit -m "initial commit" Git_Basic_Cmds.txt
		+[25042017 6ef9362] initial commit
		+ 1 file changed, 33 insertions(+)
		+ create mode 100644 Git_Basic_Cmds.txt
		+
		+satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
		+$ git status
		+On branch 25042017
		+Your branch is ahead of 'origin/25042017' by 1 commit.
		+  (use "git push" to publish your local commits)
		+nothing to commit, working tree clean
		+
		+$ git push
		+Counting objects: 3, done.
		+Delta compression using up to 4 threads.
		+Compressing objects: 100% (3/3), done.
		+Writing objects: 100% (3/3), 684 bytes | 0 bytes/s, done.
		+Total 3 (delta 1), reused 0 (delta 0)
		+remote:
		+remote: Create pull request for 25042017:
		+remote:   https://bitbucket.org/satishdevops/kb/pull-requests/new?source=25042017&t=1
		+remote:
		+To https://bitbucket.org/satishdevops/kb.git
		+   c5cf314..6ef9362  25042017 -> 25042017

###########################
$ git remote -v
origin  https://satishdevops@bitbucket.org/satishdevops/kb.git (fetch)
origin  https://satishdevops@bitbucket.org/satishdevops/kb.git (push)

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git status
On branch 25042017
Your branch is up-to-date with 'origin/25042017'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   Git_Basic_Cmds.txt

no changes added to commit (use "git add" and/or "git commit -a")

satish@satish-PC MINGW64 ~/Google Drive/Education/DEVOPS/GIT/kb (25042017)
$ git push -u origin 25042017
Everything up-to-date
Branch 25042017 set up to track remote branch 25042017 from origin.
